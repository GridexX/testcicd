<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);

        $user = User::factory()->create();
        $this->routeWithoutSession($user);
        $this->routeWithSession($user);
    }

    /**
     * Testing if the user can access route while bein connected
     * 
     * @return void
     */
    public function routeWithSession($user) {
        $response = $this->actingAs($user)
             ->withSession(['foo' => 'bar'])
             ->get('/userTest');
        
        $response->assertStatus(200);
    }

    /**
     * Route without session 
     * 
     * @return void
     */
    public function routeWithoutSession($user) {
        $response = $this->get('/userTest');
        $response->assertStatus(403);
    }
}

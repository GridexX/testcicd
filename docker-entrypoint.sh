#!/bin/bash

# Moving .env.example to .env if needed
if [ -f .env.example ]; then
    if [ -f .env ]; then
        rm .env
    fi

    cp .env.example .env
else
    cp .env.example .env
fi

# Removing composer.lock if exists
if [ -f composer.lock ]; then
    rm composer.lock
fi

php -v
php artisan key:generate
php artisan view:clear
php artisan cache:clear
php artisan route:cache
php artisan config:cache

if "$DO_STORAGE_LINK"; then
    # Removing /public/storage dir if exists, because of php artisan storage:link
    if [ -d public/storage ]; then
        rm -rf public/storage
    fi

    php artisan storage:link
fi

# Run CMD passed to container
exec "$@"



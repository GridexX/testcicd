FROM protocole/laravel:7.3

LABEL maintainer="alexandre@go2scale.com"
#  "@php artisan package:discover --ansi"

ENV APACHE_PORT 80
ENV DO_STORAGE_LINK false
ENV LARAVEL_ROOT laravel
ENV DOCUMENT_ROOT /var/www

RUN if [ ! -d ${DOCUMENT_ROOT} ]; then mkdir ${DOCUMENT_ROOT}; fi

WORKDIR ${DOCUMENT_ROOT}

RUN apk add apache2 php7-apache2

# Installing Composer modules in second, to use cache
# RUN if [ -f composer.lock ]; then rm composer.lock; fi
COPY ${LARAVEL_ROOT}/composer.json ${DOCUMENT_ROOT}
RUN sed -ri -e 's/@php artisan package:discover --ansi//g' ${DOCUMENT_ROOT}/composer.json
RUN composer install --prefer-dist --no-ansi --no-interaction --no-progress --optimize-autoloader --no-dev

# Copying laravel 
COPY ${LARAVEL_ROOT} ${DOCUMENT_ROOT}

# Copying the docker entrypoint
COPY docker-entrypoint.sh /

# Activating Modules & configuration for apache2
RUN sed -i '/LoadModule rewrite_module/s/^#//g' /etc/apache2/httpd.conf \
    && sed -i '/LoadModule session_module/s/^#//g' /etc/apache2/httpd.conf \
    && sed -i 's/AllowOverride\ None/AllowOverride\ All/g' /etc/apache2/httpd.conf \
    && sed -ri -e 's!/var/www/localhost/htdocs!${DOCUMENT_ROOT}!g' /etc/apache2/httpd.conf \
    && sed -ri -e 's/User\ apache/User\ www-data/g' /etc/apache2/httpd.conf \ 
    && sed -ri -e 's/Group\ apache/Group\ www-data/g' /etc/apache2/httpd.conf \
    && sed -ri -e 's/Listen\ 80/Listen\ ${APACHE_PORT}/g' /etc/apache2/httpd.conf 
    
# Editing rights for the files to allow apache to use them
RUN chown -R www-data:www-data ${DOCUMENT_ROOT}
# Putting the docker entrypoint executeable 
RUN chmod a+x /docker-entrypoint.sh

# Exposing port for Apache2
EXPOSE ${APACHE_PORT}

ENTRYPOINT ["/docker-entrypoint.sh"]
# Run Apache2 as server 
CMD /usr/sbin/httpd -f /etc/apache2/httpd.conf -DFOREGROUND
WORKDIR ${DOCUMENT_ROOT}